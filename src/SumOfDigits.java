/**
 * Created by Dmitry on 7/16/2015.
 */
import java.util.Scanner;

public class SumOfDigits {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String input;
        System.out.print("Enter series of digits: ");
        input = keyboard.nextLine();
        char[] arrayCh = input.toCharArray();
        int[] arrayInt = new int[arrayCh.length];
        int total = 0;
        boolean check =false;

        for (int n = 0; n < arrayCh.length; n++) {
            if (!Character.isDigit(arrayCh[n])) {
                check = false;
                break;
            } else {
                check = true;
            }
        }

        if (check) {
            for (int n = 0; n < arrayCh.length; n++) {
                arrayInt[n] = Character.getNumericValue(arrayCh[n]);
                total = total + arrayInt[n];
            }

            int min = arrayInt[0];
            int max = arrayInt[0];

            for (int n = 0; n < arrayInt.length; n++) {
                if (arrayInt[n] < min)
                    min = arrayInt[n];
                if (arrayInt[n] > max)
                    max = arrayInt[n];
            }

            System.out.print("\nSum: " + total);
            System.out.print("\nMinimum: " + min);
            System.out.print("\nMaximum: " + max);
        } else {
            System.out.print("\nWrong input!");
        }
        keyboard.close();
    }
}
